import time
import datetime
import pandas as pd


refill = pd.DataFrame(pd.read_csv('refill.csv'))
refill_log = pd.DataFrame(pd.read_csv('refill_logistic.csv'))
refill_log = refill_log[['Product code', 'Nearest Production Date']]


result = refill.merge(refill_log, left_on='product_id', right_on='Product code', how='outer')
# production_date = []
# for idx, i in result.iterrows():
#     if str(i['Nearest Production Date']) != 'nan':
#         i['Nearest Production Date'] = i['Nearest Production Date'].apply((time.mktime(datetime.datetime.strptime(str(i['Nearest Production Date']), "%Y-%m-%d").timetuple())))

result['production_date'] = result['Nearest Production Date'].apply(lambda x: time.mktime(datetime.datetime.strptime(x, "%Y-%m-%d").timetuple()) if str(x) != 'nan' else x)

result.to_csv('a.csv')

