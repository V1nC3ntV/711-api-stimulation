import json
import time
import datetime
import ast
import pandas as pd
import math
import supplier_api_request
import hq_api_requests

TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TOMORROW = int(time.mktime((datetime.datetime.today()
                            + datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE = datetime.datetime.fromtimestamp(
    TOMORROW / 1000).strftime('%Y-%m-%d')
TODAY_DATE = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
SUPPLIER_FEEDBACK = [
    "refill_id",
    "refill_detail_id",
    "supplier_id",
    "product_name",
    "product_id",
    "supplier_product_id",
    "total_order_the_last_two_weeks",
    "total_order_the_last_week",
    "current_inventory",
    "refill_uomsize",
    "refill_uomdescription",
    "base_unit_uom_description",
    "nearest_expiry_date",
    "upc",
    "delivered",
    "mif",
    "new_store_unit_quantity",
    "event_or_promotion_quantity",
    "final_suggestion",
    "forecast",
    "minimum_inventory",
    "refilled_suggestion",
    "auto_suggestion",
    "refill_uomquantity",
    "category_id",
    "category_name",
    "subcategory_id",
    "subcategory_name",
    "total_approved",
    "supplier_feed_back",
    "receiving_notes"]
SUPPLIER_FEEDBACK_DETAIL = [
    'delivery_date',
    'expiry_date',
    'quantity']


def xstr(s):
    if s != 'null':
        if s is None:
            return 'null'
        if math.isnan(s):
            return 'null'
    else:
        return s
    return int(s)


def generate_supplier_feedback(cdc_id, refill_date):
    # Get refill suggestion
    # cdc_id_pd = supplier_api_request.get_current_cdc()
    # cdc_id_pd = pd.DataFrame(cdc_id_pd)
    # cdc_id_pd = cdc_id_pd[['id']]
    # cdc_id_pd = cdc_id_pd.to_dict(orient="list")
    # cdc_id_pd = cdc_id_pd['id']

    # for i, val in enumerate(cdc_id_pd)
    refill_suggestion_pd = pd.DataFrame(
        supplier_api_request.get_supplier_refill(cdc_id, refill_date))
    supplier_feedback_pd = pd.DataFrame(columns=SUPPLIER_FEEDBACK)

    if not refill_suggestion_pd.empty:
        supplier_feedback_pd['refill_id'] = refill_suggestion_pd['refill_id']
        supplier_feedback_pd['refill_detail_id'] = refill_suggestion_pd['refill_detail_id']
        supplier_feedback_pd['supplier_id'] = refill_suggestion_pd['supplier_id']
        supplier_feedback_pd['product_name'] = refill_suggestion_pd['product_name'].astype(
            str)
        supplier_feedback_pd['product_id'] = refill_suggestion_pd['product_id']
        supplier_feedback_pd['supplier_product_id'] = refill_suggestion_pd['supplier_product_id']
        supplier_feedback_pd['total_order_the_last_two_weeks'] = refill_suggestion_pd['total_order_the_last_two_weeks']
        supplier_feedback_pd['total_order_the_last_week'] = refill_suggestion_pd['total_order_the_last_week']
        supplier_feedback_pd['current_inventory'] = refill_suggestion_pd['current_inventory']
        supplier_feedback_pd['refill_uomsize'] = refill_suggestion_pd['refill_uomsize']
        supplier_feedback_pd['refill_uomdescription'] = refill_suggestion_pd['refill_uomdescription']
        supplier_feedback_pd['base_unit_uom_description'] = refill_suggestion_pd['base_unit_uom_description']
        supplier_feedback_pd['nearest_expiry_date'] = refill_suggestion_pd['nearest_expiry_date'].apply(
            lambda x: xstr(x))
        supplier_feedback_pd['upc'] = refill_suggestion_pd['upc']
        supplier_feedback_pd['delivered'] = refill_suggestion_pd['delivered']
        supplier_feedback_pd['mif'] = refill_suggestion_pd['mif']
        supplier_feedback_pd['new_store_unit_quantity'] = refill_suggestion_pd['new_store_unit_quantity']
        supplier_feedback_pd['event_or_promotion_quantity'] = refill_suggestion_pd['event_or_promotion_quantity']
        supplier_feedback_pd['final_suggestion'] = refill_suggestion_pd['final_suggestion']
        supplier_feedback_pd['forecast'] = refill_suggestion_pd['forecast']
        supplier_feedback_pd['minimum_inventory'] = refill_suggestion_pd['minimum_inventory']
        supplier_feedback_pd['refilled_suggestion'] = refill_suggestion_pd['refilled_suggestion']
        supplier_feedback_pd['auto_suggestion'] = refill_suggestion_pd['auto_suggestion']
        supplier_feedback_pd['refill_uomquantity'] = refill_suggestion_pd['refill_uomquantity']
        supplier_feedback_pd['category_id'] = refill_suggestion_pd['category_id']
        supplier_feedback_pd['category_name'] = refill_suggestion_pd['category_name']
        supplier_feedback_pd['subcategory_id'] = refill_suggestion_pd['subcategory_id']
        supplier_feedback_pd['subcategory_name'] = refill_suggestion_pd['subcategory_name']
        supplier_feedback_pd['total_approved'] = refill_suggestion_pd['total_approved']
        # supplier_feedback_pd['supplier_feed_back'] = refill_suggestion_pd['supplier_feed_back']
        supplier_feedback_pd['receiving_notes'] = refill_suggestion_pd['receiving_notes']
        # supplier_feedback_pd['receiving_notes'] = list()

        for index, i in supplier_feedback_pd.iterrows():
            i['receiving_notes'] = []
            # if supplier_feedback_pd['nearest_expiry_date'].loc[index] is none:
            #     supplier_feedback_pd['nearest_expiry_date'].loc[index] = ''
            # else:
            #     if math.isnan(
            #             supplier_feedback_pd['nearest_expiry_date']
            #             .loc[index]) is true:
            #         supplier_feedback_pd['nearest_expiry_date'].loc[index] = ('')
            #     else:
            #         supplier_feedback_pd['nearest_expiry_date'].loc[index] = (
            #             int(supplier_feedback_pd['nearest_expiry_date']
            #                 .loc[index]))

            supplier_feedback_detail = """
            {{\"delivery_date\":{0},
            \"expiry_date\":{1},
            \"quantity\":{2},
            \"refill_detail_id\":{3}}}""".format(
                TOMORROW,
                xstr(supplier_feedback_pd['nearest_expiry_date'].loc[index]),
                str(supplier_feedback_pd['final_suggestion'].loc[index]),
                supplier_feedback_pd['refill_detail_id'].loc[index])
            supplier_feedback_detail = "".join(
                supplier_feedback_detail.split())
            print(supplier_feedback_detail)
            supplier_feedback_detail = json.loads(supplier_feedback_detail)

            for key in supplier_feedback_detail:
                if supplier_feedback_detail[key] is None:
                    supplier_feedback_detail[key] = "null"

            supplier_feedback_pd['supplier_feed_back'].loc[index] = [
                supplier_feedback_detail]

        supplier_feedback_json = {
            "delivery_date": TOMORROW_DATE,
            "refill_details": json.loads(supplier_feedback_pd.to_json(
                orient="records"))}
        print(supplier_feedback_json)
        print(
            supplier_api_request
            .put_supplier_refill_feedback(supplier_feedback_json))

        submit_feedback = "{{'cdc_id': {}, 'delivery_date': '{}'}}".format(
            cdc_id, TOMORROW_DATE)
        print(submit_feedback)
        print(
            supplier_api_request
            .post_submit_feedback_to_approve(
                ast.literal_eval(submit_feedback)))


def put_feedback_supplier():
    list_suppliers_df = hq_api_requests.get_suppliers_list()
    list_suppliers_df = pd.DataFrame(list_suppliers_df)

    user_supplier = """{{\"email\":\"hoangviet@siliconstraits.com\",\"user_role\":3,\
    \"supplier_id\":{0}}}"""
    user_supplier = "".join(user_supplier.split())
    print(user_supplier)

    for index, supplier in list_suppliers_df.iterrows():
        supplier = json.loads(supplier.to_json())
        user_supplier_json = user_supplier.format(supplier['id'])
        # user_supplier_json = ast.literal_eval(user_supplier_json)
        print(hq_api_requests.put_user_supplier(
            210, ast.literal_eval(user_supplier_json)))
        print('start send supplier feeback:')
        print(generate_supplier_feedback(3, '2018-02-26'))


# print(generate_supplier_feedback(3, '2017-08-21'))
put_feedback_supplier()
