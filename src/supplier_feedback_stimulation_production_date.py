import json
import time
import datetime
import ast
import pandas as pd
import math
import supplier_api_request
import hq_api_requests

TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TOMORROW = int(time.mktime((datetime.datetime.today()
                            + datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE = datetime.datetime.fromtimestamp(
    TOMORROW / 1000).strftime('%Y-%m-%d')
TODAY_DATE = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
SUPPLIER_FEEDBACK = [
    'refill_detail_id',
    'product_id',
    'delivery_group_id',
    'final_suggestion',
    'expiry_date',
    'production_date',
    'quantity']


def xstr(s):
    if s != 'null':
        if s is None:
            return 'null'
        if math.isnan(s):
            return 'null'
    else:
        return s
    return int(s)


def generate_supplier_feedback(cdc_id, refill_date):
    # for i, val in enumerate(cdc_id_pd)
    refill_suggestion_pd = pd.DataFrame(
        supplier_api_request.get_supplier_refill(cdc_id, refill_date))
    supplier_feedback_pd = pd.DataFrame(columns=SUPPLIER_FEEDBACK)

    if not refill_suggestion_pd.empty:
        supplier_feedback_pd['refill_detail_id'] = refill_suggestion_pd['refill_detail_id']
        supplier_feedback_pd['product_id'] = refill_suggestion_pd['product_id']
        supplier_feedback_pd['delivery_group_id'] = refill_suggestion_pd['delivery_group_id']
        supplier_feedback_pd['quantity'] = refill_suggestion_pd['final_suggestion']
        supplier_feedback_pd['expiry_date'] = refill_suggestion_pd['nearest_expiry_date'].apply(lambda x: xstr(x))
        # print(supplier_feedback_pd['expiry_date'])
        # supplier_feedback_pd['production_date'] = supplier_feedback_pd.apply(
        #     lambda row: supplier_api_request.get_exp_date_by_production_date(row['product_id'], row['expiry_date'])['expiry_date'], axis=1)
        supplier_feedback_pd['production_date'] = '1519966336629'
        # print(supplier_feedback_pd['production_date'])
        # supplier_feedback_pd['production_date'] = supplier_feedback_pd['production_date'].apply(lambda x: xstr(x))

        supplier_feed_back = []
        for index, i in supplier_feedback_pd.iterrows():
            # print(i['quantity'])
            supplier_feed_back.append(ast.literal_eval("[{{'id':'null','expiry_date':'{}','production_date':'{}','quantity':'{}'}}]".format(
                i['expiry_date'],
                i['production_date'],
                i['quantity']
            )))

        supplier_feedback_pd['supplier_feed_back'] = supplier_feed_back

        feedback_json = supplier_feedback_pd[['refill_detail_id','supplier_feed_back']]

        supplier_feedback_json = {
            "delivery_date": TOMORROW_DATE,
            "delivery_group_ids": ['null'],
            "refill_details": json.loads(feedback_json.to_json(
                orient="records"))}
        print(supplier_feedback_json)
        print(
            supplier_api_request
            .put_supplier_refill_feedback(supplier_feedback_json))

        submit_feedback = "{{'delivery_group_ids':[None], 'cdc_id': {}, 'delivery_date': '{}'}}".format(
            cdc_id, TOMORROW_DATE)
        print(submit_feedback)
        print(
            supplier_api_request
            .post_submit_feedback_to_approve(
                ast.literal_eval(submit_feedback)))


def put_feedback_supplier():
    list_suppliers_df = hq_api_requests.get_suppliers_list()
    list_suppliers_df = pd.DataFrame(list_suppliers_df)

    user_supplier = """{{\"email\":\"hoangviet@siliconstraits.com\",\"user_role\":3,\
    \"supplier_id\":{0}}}"""
    user_supplier = "".join(user_supplier.split())
    print(user_supplier)

    for index, supplier in list_suppliers_df.iterrows():
        supplier = json.loads(supplier.to_json())
        user_supplier_json = user_supplier.format(supplier['id'])
        # user_supplier_json = ast.literal_eval(user_supplier_json)
        print(hq_api_requests.put_user_supplier(
            210, ast.literal_eval(user_supplier_json)))
        print('start send supplier feeback:')
        print(generate_supplier_feedback(3, '2018-04-02'))


# print(generate_supplier_feedback(3, '2018-01-22'))
put_feedback_supplier()
