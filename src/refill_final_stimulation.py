import json
import time
import datetime
import pandas as pd
import logistic_api_request

TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TOMORROW = int(time.mktime((datetime.datetime.today() +
                            datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE = datetime.datetime.fromtimestamp(
    TOMORROW / 1000).strftime('%Y-%m-%d')
TODAY_DATE = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')
REFILL_FINAL = [
    "refill_id",
    "refill_detail_id",
    "supplier_id",
    "product_id",
    "mif",
    "new_store_unit_quantity",
    "event_or_promotion_quantity",
    "final_suggestion",
    "nearest_expiry_date",
    "forecast",
    "minimum_inventory",
    "refilled_suggestion",
    "auto_suggestion",
    "refill_uomquantity"]


def get_logistic_refill(refill_date):
    logistic_supplier_pd = logistic_api_request.get_logistic_refill_by_suppliers(
        refill_date)
    logistic_supplier_pd = pd.DataFrame(logistic_supplier_pd)
    # print(logistic_supplier_pd)

    pd.options.display.float_format = '{:,.0f}'.format
    refill_final_pd = pd.DataFrame()
    for index, row in logistic_supplier_pd.iterrows():
        supplier_info = json.loads(row.to_json())
        logistic_refill_json = logistic_api_request.get_single_logistic_refill(
            supplier_info['cdc_id'], supplier_info['supplier_id'], refill_date)
        logistic_refill_pd = pd.DataFrame(logistic_refill_json)

        refill_temp_pd = pd.DataFrame(columns=REFILL_FINAL)
        refill_temp_pd['refill_id'] = logistic_refill_pd['refill_id']
        refill_temp_pd['refill_detail_id'] = logistic_refill_pd['refill_detail_id'].astype(
            int)
        refill_temp_pd['supplier_id'] = logistic_refill_pd['supplier_id'].astype(
            int)
        refill_temp_pd['product_id'] = logistic_refill_pd['product_id'].astype(
            int)
        refill_temp_pd['mif'] = logistic_refill_pd['mif']
        refill_temp_pd['new_store_unit_quantity'] = logistic_refill_pd['new_store_unit_quantity']
        refill_temp_pd['event_or_promotion_quantity'] = logistic_refill_pd['event_or_promotion_quantity']
        refill_temp_pd['final_suggestion'] = (
            logistic_refill_pd['refill_uomsize'] * 1000).astype(int)
        refill_temp_pd['nearest_expiry_date'] = logistic_refill_pd['nearest_expiry_date']
        refill_temp_pd['forecast'] = logistic_refill_pd['forecast'].astype(int)
        refill_temp_pd['minimum_inventory'] = logistic_refill_pd['minimum_inventory'].astype(
            int)
        refill_temp_pd['refilled_suggestion'] = refill_temp_pd['final_suggestion']
        refill_temp_pd['auto_suggestion'] = refill_temp_pd['final_suggestion']
        refill_temp_pd['refill_uomquantity'] = logistic_refill_pd['refill_uomquantity'].astype(
            int)

        refill_temp_pd['new_store_unit_quantity'].fillna(value=0, inplace=True)
        refill_temp_pd['event_or_promotion_quantity'].fillna(
            value=0, inplace=True)
        refill_temp_pd['new_store_unit_quantity'] = refill_temp_pd['new_store_unit_quantity'].astype(
            int)

        # print(refill_temp_pd.to_json())
        refill_final_pd = refill_final_pd.append(refill_temp_pd)
        refill_temp_pd.reset_index(drop=True)
        # refill_json = refill_temp_pd.to_json(orient="records")
        # print(refill_json)
    refill_final_pd.reset_index(drop=True)
    # refill_final_pd.set_index('refill_id')
    result_not_null = pd.DataFrame(
        refill_final_pd[pd.notnull(refill_final_pd.nearest_expiry_date)])
    result_is_null = pd.DataFrame(
        refill_final_pd[pd.isnull(refill_final_pd.nearest_expiry_date)])

    result_not_null['nearest_expiry_date'] = result_not_null['nearest_expiry_date'].astype(
        int)

    result_not_null = result_not_null.to_json(orient="records")
    result_is_null = result_is_null.to_json(orient="records")
    return result_not_null, result_is_null


refill_not_null_json, refill_is_null_json = get_logistic_refill('2017-12-18')
print(logistic_api_request.put_logistic_refill_final(refill_not_null_json))
print(logistic_api_request.put_logistic_refill_final(refill_is_null_json))
