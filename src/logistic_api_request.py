import json
import requests
import config

# HQ_BACKEND             = 'https://msv-testing.local:8888'
# HQ_BACKEND             = 'https://qa-hq-backend.seven-system.com'
# HQ_BACKEND               = 'https://staging-backend.sevensystem.vn'
# HQ_BACKEND             = 'http://localhost:8888'
# HQ_BACKEND             = 'http://dev-hq-backend.seven-system.com'
# HQ_BACKEND             = 'https://uat-hq-backend.seven-system.com'
HQ_BACKEND             = config.DEFAULT_ENV['hq-backend']
# HQ_BACKEND             = 'http://10.147.1.56:8888'
LOGISTIC_AUTHENTICATION  = HQ_BACKEND + '/api/v2/portal/logistics/authentication'
LOGISTIC_REFILL          = HQ_BACKEND + '/api/v2/portal/logistics/refill/details'
LOGISTIC_REFILL_SUPPLIER = HQ_BACKEND + '/api/v2/portal/logistics/refill/suppliers'

EMAIL = 'logistics@ssv.com'
PASSWORD = '123456'
ACCESS_TOKEN = ''


def post_access():
    '''Post access and return access key'''
    access_json = "{{\"email\": \"{0}\", \"password\": \"{1}\"}}".format(
        EMAIL, PASSWORD)
    global ACCESS_TOKEN
    if not ACCESS_TOKEN:
        respone = requests.post(
            LOGISTIC_AUTHENTICATION, None, json=json.loads(access_json))

        if respone.raise_for_status():
            respone.raise_for_status()
        else:
            respone = respone.json()

        access_token = respone['access_token']
        ACCESS_TOKEN = respone['access_token']
    else:
        access_token = ACCESS_TOKEN

    # access_token = respone['access_token']
    return access_token


def get_single_logistic_refill(cdc_id, supplier_id, refill_date):
    '''Get Logistic refill info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = LOGISTIC_REFILL + '?cdc_id={}&supplier_id={}&refill_date={}'.format(cdc_id, supplier_id, refill_date)
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()['refill_detail_dto_list']
    return content


def get_logistic_refill_by_suppliers(refill_date):
    '''Get Logistic refill info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = LOGISTIC_REFILL_SUPPLIER + '?refill_date={}'.format(refill_date)
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()
    return content


def put_logistic_refill_final(refill_final_json):
    '''Put Logistic refill final info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.put(
        LOGISTIC_REFILL,
        headers=json.loads(header),
        data=refill_final_json)
    return respone.text
