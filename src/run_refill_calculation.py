import requests
import time
import datetime
import json

TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TOMORROW = int(time.mktime((datetime.datetime.today() +
                            datetime.timedelta(days=1)).timetuple()) * 1000)
TOMORROW_DATE = datetime.datetime.fromtimestamp(
    TOMORROW / 1000).strftime('%Y-%m-%d')
TODAY_DATE = datetime.datetime.fromtimestamp(TODAY / 1000).strftime('%Y-%m-%d')

url = "http://qa-hq-backend.seven-system.com/api/system/authentication"

payload = "{\n    \"email\": \"sysadmin@ssv.com\",\n    \"password\": \"vKWJNDlwo5of\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache"
    }

response = requests.request("POST", url, data=payload, headers=headers)
response = json.loads(response.text)

print(response)

url_refill = "http://qa-hq-backend.seven-system.com/api/system/refills/trigger_calculator"

querystring = {"ignore_inventory":"true","refill_date":TODAY_DATE}

headers = {
    'authorization': "bearer " + response['access_token'],
    'cache-control': "no-cache"
    }

response = requests.request("GET", url_refill, headers=headers, params=querystring)

print(response.text)