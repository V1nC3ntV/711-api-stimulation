import json
import requests
import config

# HQ_BACKEND             = 'http://10.147.1.56:8888'
# HQ_BACKEND = 'http://msv-testing.local:8888'
# HQ_BACKEND = 'http://qa-hq-backend.seven-system.com'
# HQ_BACKEND = 'https://staging-backend.sevensystem.vn'
# HQ_BACKEND = 'http://localhost:8888'
# HQ_BACKEND = 'http://dev-hq-backend.seven-system.com'
# HQ_BACKEND = 'https://uat-hq-backend.seven-system.com'
HQ_BACKEND = config.DEFAULT_ENV['hq-backend']
SUPPLIER_AUTHENTICATION = HQ_BACKEND + '/api/v2/portal/supplier/authentication'
# SUPPLIER_REFILL = HQ_BACKEND + '/api/v2/portal/supplier/refill/details'
SUPPLIER_REFILL_DETAIL = HQ_BACKEND + '/api/v2/portal/supplier/refill/details'
SUPPLIER_REFILL = HQ_BACKEND + '/api/v2/portal/supplier/refill/submit_feedback'
CURRENT_CDC = HQ_BACKEND + '/api/v2/portal/supplier/refill/current_cdcs'
SUBMIT_FEEDBACK = HQ_BACKEND + '/api/v2/portal/supplier/refill/submit_feedback_to_approve'
EXP_DATE_BY_PRODUCTION_DATE = HQ_BACKEND + '/api/v2/portal/supplier/exp_date_by_prod_date?product_id=3705&date=1506790800000'

EMAIL = 'hoangviet@siliconstraits.com'
PASSWORD = '12345678'
ACCESS_TOKEN = ''


def post_access():
    '''Post access and return access key'''
    access_json = "{{\"email\": \"{0}\", \"password\": \"{1}\"}}".format(
        EMAIL, PASSWORD)
    # global ACCESS_TOKEN
    # if not ACCESS_TOKEN:
    respone = requests.post(
        SUPPLIER_AUTHENTICATION, None, json=json.loads(access_json))

    if respone.raise_for_status():
        respone.raise_for_status()
    else:
        respone = respone.json()

    access_token = respone['access_token']
    # ACCESS_TOKEN = respone['access_token']
    # else:
    # access_token = ACCESS_TOKEN

    # access_token = respone['access_token']
    return access_token


def get_supplier_refill(cdc_id, refill_date):
    '''Get Logistic refill info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = SUPPLIER_REFILL_DETAIL + '?cdc_id={}&refill_date={}'.format(
        cdc_id, refill_date)
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()['refill_detail_dto_list']
    return content


def get_current_cdc():
    '''Get current cdc info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        CURRENT_CDC,
        headers=json.loads(header))
    content = respone.json()
    return content


def put_supplier_refill_feedback(refill_feedback_json):
    '''Put Supplier refill feedback info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.put(
        SUPPLIER_REFILL,
        headers=json.loads(header),
        json=refill_feedback_json)
    return respone.text


def post_submit_feedback_to_approve(json_info):
    '''Post feedback to approve'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.post(
        SUBMIT_FEEDBACK,
        headers=json.loads(header),
        json=json_info)
    return respone.text


def get_exp_date_by_production_date(product_id, date):
    '''Get exp date by production date'''
    if date != 'null' and product_id != 'null':
        _query = '{{"product_id":{},"date":{}}}'.format(product_id, date)
        access_token = post_access()
        header = "{{\"content-type\": \"application/json\",\"authorization\":\
        \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
            access_token)
        respone = requests.request(
            "GET",
            EXP_DATE_BY_PRODUCTION_DATE,
            headers=json.loads(header),
            params=_query)
        content = respone.json()
        return content
    else:
        return None
